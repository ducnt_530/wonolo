require "instagram"

class PostsController < ApplicationController
  # Config Instagram
  Instagram.configure do |config|
    config.client_id = "f8d8ffa706724bcebd41a57856fe0ee0"
    config.client_secret = "507fd2246d0b4fff8c3fdd37b843552d"
  end

  # Search posts by location and distance
  def search
    @title = 'Search Medias'
  end

  # Get medias from instagram
  # @method: Ajax POST
  # @param: lat (required)
  # @param: lng (required)
  # @param: distance (required), meters, default 1000, max: 5000
  # @param: max_timestamp (optional), a unix timestamp. All media returned will be taken earlier than this timestamp.
  # @return: Json object
  def get_medias
    # Get distance params
    number_of_records = $CONFIG.items_per_page

    # Validate params
    validate = true
    messages = {}
    if params[:lat].blank?
      validate = false
      messages[:lat] = "Latitude is required."
    end

    if params[:lng].blank?
      validate = false
      messages[:lng] = "Logitude is required."
    end

    if params[:distance].blank? || params[:distance].to_f < 500
      validate = false
      messages[:distance] = "Distance is required."
    end

    if !validate
      render :json => {status: 0, error: messages}, :status => :ok and return
    end

    # Init data object
    data = []

    # Check has load more
    has_more = false

    begin
      # For load more
      if !params[:max_timestamp].blank?
        data = Instagram.media_search(
          params[:lat].to_s, 
          params[:lng].to_s, 
          {
            :count => number_of_records + 1, 
            :distance => params[:distance],
            :max_timestamp => params[:max_timestamp].to_i
          })
        data.shift if data.count > 0
      else
        # First page
        data = Instagram.media_search(
          params[:lat].to_s, 
          params[:lng].to_s, 
          {
            :count => number_of_records, 
            :distance => params[:distance]
          })
      end

      # Check has load more
      if data.count == number_of_records
        has_more = true
      end

    rescue Exception => exc
      puts "Error: " + exc.inspect
    end
    render :json => {status: 1, data: data, has_more: has_more}, :status => :ok and return
  end

end
