// Init local params
var loading = false; // Prevent send too requests
var max_timestamp = "" // Store created time of last media

// Main Handle
$(document).ready(function() {
  // Focus to address search
  $("#address").focus();

  // Submit search form
  // $('.btn-geocode').click(function() {
  //   geocode();
  // });

  // Submit search form
  $('.btn-search').click(function() {
    // Clear data
    $("#list-medias").html("");
    $(".show-more").hide();

    // Send ajax request to get medias for fist page
    max_timestamp = "";

    // Search medias
    search(max_timestamp);
  });

  $("#address, #lat, #lng").keypress(function(e) {
    if(e.which == 13) {
      $(".btn-search").trigger("click");
    }
  });

  // Load More medias
  $(document).on("click", ".show-more",function() {
    getMedias($("#lat").val(), $("#lng").val(), $("#distance").val(), max_timestamp);
  });

  // View detail media with blueimp plugin
  $(document).on("click", "#list-medias", function (event) {
    event = event || window.event;
    var target = event.target || event.srcElement,
        link = target.src ? target.parentNode : target,
        options = {index: link, event: event},
        links = this.getElementsByTagName('a');
    blueimp.Gallery(links, options);
  });

  // hide #back-top first
  $("#back-top").hide();
  
  // fade in #back-top
  $(function () {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 100) {
        $('#back-top').fadeIn();
      } else {
        $('#back-top').fadeOut();
      }
    });

    // scroll body to 0px on click
    $('#back-top a').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 800);
      return false;
    });
  });

});

// Send ajax get medias
function getMedias(lat, lng, distance, local_max_timestamp) {
  if (!loading) {
    $.ajax({
      type: "POST",
      url: "/get_medias",
      dataType: 'Json',
      data: {
        lat: lat,
        lng: lng,
        distance: distance,
        max_timestamp: local_max_timestamp
      },
      beforeSend: function() {
        $('.error-text').html('');
        $('#load-more').show();
        loading = true;
      },
      success: function(response) {
        if (response.status == 1) {
          // Show/hide paging
          if (response.has_more) {
            $(".show-more").show();
          } else {
            $(".show-more").hide();
          }

          var data = response.data;
          if (data.length > 0) {
            for (i in data) {
              var profile_picture = data[i].user.profile_picture;
              var username = data[i].user.username;
              var created_time = timeConverter(data[i].created_time);
              var standard_resolution = data[i].images.standard_resolution.url;
              var low_resolution = data[i].images.low_resolution.url;
              var thumbnail = data[i].images.thumbnail.url;
              var item = "<div class='col-sm-3'>" +
                "<div class='item'>" + 
                  "<div class='item-header'>" +
                    "<div class='item-owner'>" +
                      "<img src='" + profile_picture + "'/></div>" +
                    "<div class='item-owner-content'>" +
                      "<div class='item-owner-name'>" + username + "</div>" +
                      "<div class='item-time'>" + created_time + "</div>" +
                    "</div>" + 
                  "</div>" +
                  "<div class='item-body'>" +
                    "<a href='" + standard_resolution + "'>" +
                      "<img src='" + low_resolution + "'/>" +
                    "</a>" +
                  "</div>" + 
                "</div></div>";

              $("#list-medias").append(item);
              
              // show location of media on map
              var contentInfowindow = "<div><b>" + username + "</b></div>" + 
                  "<div style='font-size: 10px; margin-bottom: 5px;'>" + created_time + "</div>" + 
                  "<div><img style='max-width: 100px; max-height: 100px;' src='" + thumbnail + "'/></div>";
              var latlng = new google.maps.LatLng(data[i].location.latitude, data[i].location.longitude);
              addMarker(latlng, data[i].user.username, contentInfowindow);

            }
            // Set value for action: next, previous page
            max_timestamp = data[data.length - 1].created_time 

            // Scroll to list medias, only for fist request
            if (local_max_timestamp == '') {
              // Scroll to list medias
              $('html, body').animate({
                  scrollTop: $("#list-medias").offset().top - 205
              }, 2000);
              return false;
            }
          } else {
            $("#list-medias").append("<div class='text-center empty-data'>No media found.</div>")
          }
        } else {
          var error_lat = response.error.lat;
          var error_lng = response.error.lng;
          var error_distance = response.error.distance;
          if (typeof error_lat != 'undefined') {
            $("#lat").siblings('.error-text').html(error_lat);
          }
          if (typeof error_lng != 'undefined') {
            $("#lng").siblings('.error-text').html(error_lng);
          }
          if (typeof error_distance != 'undefined') {
            $("#distance").siblings('.error-text').html(error_distance);
          }

        }
      },
      complete: function() {
        $('#load-more').hide();
        loading = false;
      }
    });
  }
}

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp*1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var time = date + ', ' + month + ' ' + year + ' ' + hour + ':' + min;
  return time;
}

// Map Handle
var geocoder;
var map;
var markers = [];
function initialize() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(21.0277644, 105.83415979999995);
  var mapOptions = {
    zoom: 13,
    center: latlng
  }
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
}

function search(max_timestamp) {
  var address = $("#address").val();
  if (address != '') { // Find location for address
    // Delete all markers
    deleteMarkers();

    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        var location = results[0].geometry.location;
        map.setCenter(location);
        //addMarker(location);
        
        // Update lat, lng value, use for search more
        $("#lat").val(location.lat());
        $("#lng").val(location.lng());
        getMedias(location.lat(), location.lng(), $("#distance").val(), max_timestamp);
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  } else {
    getMedias($("#lat").val(), $("#lng").val(), $("#distance").val(), max_timestamp);
  }
  
}

// Add Marker
function addMarker(location, title, contentString) {
  var marker = new google.maps.Marker({
      map: map,
      position: location,
      title: title
  });
  markers.push(marker);

  // Add infowindow content
  var infowindow = new google.maps.InfoWindow({
      content: contentString
  });
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map, marker);
  });
}

// Sets the map on all markers in the array.
function setAllMap(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  setAllMap(null);
  markers = [];
}

google.maps.event.addDomListener(window, 'load', initialize);