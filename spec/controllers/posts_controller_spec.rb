require "rails_helper"

RSpec.describe PostsController, :type => :controller do
  # Test for search view
  describe "GET #search" do
    it "responds successfully with an HTTP 200 status code" do
      get :search
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "renders the index template" do
      get :search
      expect(response).to render_template("search")
    end
  end

  # Test for get medias
  describe "GET #get_medias" do
  	it "responds not found with an HTTP 404 status code" do
      post :get_medias
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it "responds with JSON" do
    	post :get_medias
    	response.header['Content-Type'].should include 'application/json'
    end

    # it "missing params" do
    #  	post :get_medias, {:params => {:lat => '40.7143528', :lng => '-74.00597309999999', :distance => 1000}}
    #  	controller.params[:lat].should_not be_nil
    #  	controller.params[:lat].should eql '40.7143528'
    #  	controller.params[:lng].should_not be_nil
    #  	controller.params[:lng].should eql '-74.00597309999999'
    #  	controller.params[:distance].should_not be_nil
    #  	controller.params[:distance].should eql 1000
    # end
   
  end

end