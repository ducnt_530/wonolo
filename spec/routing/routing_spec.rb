require 'rails_helper'

RSpec.describe "routing to root", :type => :routing do
  it "routes / with GET method" do
    expect(:get => "/").to route_to(
      :controller => "posts",
      :action => "search"
    )
  end

  it "route not found for / with post method" do
    expect(:post => "/").not_to be_routable
  end

  it "routes /get_medias with POST method" do
    expect(:post => "/get_medias").to route_to(
      :controller => "posts",
      :action => "get_medias"
    )
  end

  it "route not found for /get_medias with get method" do
    expect(:get => "/get_medias").not_to be_routable
  end
end